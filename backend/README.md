SkillDB::Backend
===================
Dieses Maven-Modul enthält das Backend der Anwendung.
Jenes ist eine Spring Boot Anwendung und kann z.B. über folgendes Kommando gestartet werden:
```
cd backend
export SPRING_PROFILES_ACTIVE=dev
mvn spring-boot:run 
```