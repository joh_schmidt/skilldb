package org.devshred.skilldb.crawler;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.OkUrlFactory;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.kohsuke.github.RateLimitHandler;
import org.kohsuke.github.extras.OkHttpConnector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

/**
 * Configuration of the GitHub API Client.
 * http://github-api.kohsuke.org
 * <p>
 * Using OkHttp to cache requests.
 */
@Configuration
public class CrawlerConfiguration {
    @Value("${github.endpoint:https://api.github.com}")
    private String githubEndpoint;

    @Value("${github.fileCache:/tmp/ok_cache}")
    private String fileCache;

    @Bean
    public GitHub gitHub() throws IOException {
        final File cacheDir = new File(fileCache);
        final Cache cache = new Cache(cacheDir, 10 * 1024 * 1024); // 10MB cache
        return new GitHubBuilder().withEndpoint(githubEndpoint)
                .withConnector(new OkHttpConnector(new OkUrlFactory(new OkHttpClient().setCache(cache))))
                .withRateLimitHandler(RateLimitHandler.FAIL)
                .build();
    }
}
