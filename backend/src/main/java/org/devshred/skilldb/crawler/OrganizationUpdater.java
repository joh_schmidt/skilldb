package org.devshred.skilldb.crawler;

import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.devshred.skilldb.domain.Organization;
import org.devshred.skilldb.domain.User;
import org.devshred.skilldb.repository.OrganizationRepository;
import org.kohsuke.github.GHUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Updates the SkillDB.
 * If an entity dosen't exist at the database or is outdated,
 * a new request to the GitHub API will be send via {@link GitHubCrawler}.
 * <p>
 * This service updates the {@link Organization} and triggers the update of
 * {@link User}s in an asynchronous manner.
 */
@Service
@Slf4j
public class OrganizationUpdater {
    private final GitHubCrawler gitHubCrawler;

    private final OrganizationRepository organizationRepository;

    private final UserUpdater userUpdater;

    @Autowired
    public OrganizationUpdater(final GitHubCrawler gitHubCrawler, final OrganizationRepository organizationRepository, final UserUpdater userUpdater) {
        this.gitHubCrawler = gitHubCrawler;
        this.organizationRepository = organizationRepository;
        this.userUpdater = userUpdater;
    }

    public void insertOrUpdateOrganization(final String organizationName) {
        final Stopwatch stopwatch = Stopwatch.createStarted();

        final Optional<Organization> orgOptional = organizationRepository.findByName(organizationName);
        final Organization organization;
        if (orgOptional.isPresent()) {
            log.debug("organization {} already exists", organizationName);
            organization = orgOptional.get();
        } else {
            log.info("create organization {}", organizationName);
            organization = organizationRepository.save(new Organization(organizationName));
        }

        if (organization.isOutdated()) {
            log.info("updating organization {}", organizationName);

            try {
                final Collection<GHUser> members = gitHubCrawler.getMembersOfOrganization(organizationName);

                final List<CompletableFuture<ProcessingResult>> userFutures = members.stream()
                        .map(user -> userUpdater.insertOrUpdateUser(user, organization))
                        .collect(Collectors.toList());

                final CompletableFuture<Long> countFuture =
                        // create a combined Future
                        CompletableFuture.allOf(userFutures.toArray(new CompletableFuture[0]))
                                // get the results if all Futures are completed
                                .thenApply(v -> userFutures.stream()
                                        .map(CompletableFuture::join)
                                        // count all Futures with result FAILED
                                        .filter(result -> result.equals(ProcessingResult.FAILED))
                                        .count());

                log.info("{} of {} users failed to process", countFuture.get(), userFutures.size());

                if (countFuture.get() == 0) {
                    organization.markAsUpdated();
                    organizationRepository.save(organization);
                    log.info("finished update of organization {} in {}ms", organizationName, stopwatch.elapsed(MILLISECONDS));
                } else {
                    log.warn("failed to update organization {}", organizationName);
                }
            } catch (IOException | ExecutionException | InterruptedException e) {
                log.error("failed to update organization {} due to: {}", organizationName, e.getMessage());
            }
        } else {
            log.info("organization {} is up to date", organizationName);
        }
    }
}
