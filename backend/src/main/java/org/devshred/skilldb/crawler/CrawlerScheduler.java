package org.devshred.skilldb.crawler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Triggers the update of the SkillDB.
 * Runs hourly (since GitHubs rate limit also resets hourly).
 */
@Component
@Profile("!test")
public class CrawlerScheduler {
    private final OrganizationUpdater organizationUpdater;

    @Value("${skilldb.organization:codecentric}")
    private String organizationName;

    @Autowired
    public CrawlerScheduler(final OrganizationUpdater organizationUpdater) {
        this.organizationUpdater = organizationUpdater;
    }

    @Scheduled(fixedDelay = 3600_000)
    public void triggerUpdate() {
        organizationUpdater.insertOrUpdateOrganization(organizationName);
    }
}
