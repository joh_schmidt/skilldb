package org.devshred.skilldb.crawler;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.kohsuke.github.GHException;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GHUser;
import org.kohsuke.github.GitHub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;

/**
 * Triggers the GitHub API REST Client.
 */
@Service
@Slf4j
public class GitHubCrawler {
    private final GitHub gitHub;

    @Autowired
    public GitHubCrawler(final GitHub gitHub) {
        this.gitHub = gitHub;
    }

    public Collection<GHUser> getMembersOfOrganization(final String organizationName) throws IOException {
        return gitHub.getOrganization(organizationName).listMembers().asSet();
    }

    public Collection<GHRepository> getReposOfUser(final GHUser ghUser) {
        return ghUser.listRepositories().asSet();
    }

    @HystrixCommand(fallbackMethod = "getLanguagesFallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000"),
            @HystrixProperty(name = "execution.isolation.strategy", value = "SEMAPHORE"),
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "2")
    })
    public Set<String> getLanguagesOfRepo(final GHRepository ghRepository) throws IOException {
        return ghRepository.listLanguages().keySet();
    }

    public Set<String> getLanguagesFallback(final GHRepository ghRepository) {
        throw new GHException("getLanguagesOfRepo failed");
    }
}
