package org.devshred.skilldb.crawler;

import lombok.extern.slf4j.Slf4j;
import org.devshred.skilldb.domain.GitHubRepo;
import org.devshred.skilldb.domain.Organization;
import org.devshred.skilldb.domain.User;
import org.devshred.skilldb.repository.UserRepository;
import org.kohsuke.github.GHException;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GHUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Updates the SkillDB.
 * If an entity dosen't exist at the database or is outdated,
 * a new request to the GitHub API will be send via {@link GitHubCrawler}.
 * <p>
 * This service updates the {@link User} and triggers the update of
 * {@link GitHubRepo}s in a asynchronous manner.
 */
@Service
@Slf4j
public class UserUpdater {
    private final GitHubCrawler gitHubCrawler;

    private final UserRepository userRepository;

    private final GitHubRepoUpdater gitHubRepoUpdater;

    @Autowired
    public UserUpdater(final GitHubCrawler gitHubCrawler, final UserRepository userRepository, final GitHubRepoUpdater gitHubRepoUpdater) {
        this.gitHubCrawler = gitHubCrawler;
        this.userRepository = userRepository;
        this.gitHubRepoUpdater = gitHubRepoUpdater;
    }

    @Async("userExecutor")
    public CompletableFuture<ProcessingResult> insertOrUpdateUser(GHUser ghUser, Organization organization) {
        final Optional<User> userOptional = userRepository.findByName(ghUser.getLogin());
        final User user;
        if (userOptional.isPresent()) {
            log.debug("user {} already exists", ghUser.getLogin());
            user = userOptional.get();
        } else {
            log.info("create user {}", ghUser.getLogin());
            user = userRepository.save(new User(ghUser.getLogin(), organization));
        }

        if (user.isOutdated()) {
            log.info("updating user {}", ghUser.getLogin());

            try {
                final Collection<GHRepository> repos = gitHubCrawler.getReposOfUser(ghUser);
                final List<CompletableFuture<ProcessingResult>> repoFutures = repos.stream()
                        .map(repo -> gitHubRepoUpdater.insertOrUpdateRepo(repo, user))
                        .collect(Collectors.toList());

                final CompletableFuture<Long> failureCounter =
                        // create a combined Future
                        CompletableFuture.allOf(repoFutures.toArray(new CompletableFuture[0]))
                                // get the results if all Futures are completed
                                .thenApply(v -> repoFutures.stream()
                                        .map(CompletableFuture::join)
                                        // count all Futures with result FAILED
                                        .filter(result -> result.equals(ProcessingResult.FAILED))
                                        .count());

                log.info("{} of {} repos failed to process", failureCounter.get(), repoFutures.size());

                if (failureCounter.get() == 0) {
                    user.markAsUpdated();
                    userRepository.save(user);
                    log.info("finished update of user {}", ghUser.getLogin());
                    return CompletableFuture.completedFuture(ProcessingResult.SUCCESS);
                } else {
                    log.warn("failed to update user {}", ghUser.getLogin());
                    return CompletableFuture.completedFuture(ProcessingResult.FAILED);
                }
            } catch (InterruptedException | ExecutionException | GHException e) {
                log.error("failed to evaluate results due to: {}", e.getMessage());
                return CompletableFuture.completedFuture(ProcessingResult.FAILED);
            }
        } else {
            log.info("user {} is up to date", ghUser.getLogin());
        }

        return CompletableFuture.completedFuture(ProcessingResult.SUCCESS);
    }
}
