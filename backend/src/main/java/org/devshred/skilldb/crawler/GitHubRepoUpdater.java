package org.devshred.skilldb.crawler;

import lombok.extern.slf4j.Slf4j;
import org.devshred.skilldb.domain.GitHubRepo;
import org.devshred.skilldb.domain.User;
import org.devshred.skilldb.repository.GitHubRepoRepository;
import org.kohsuke.github.GHException;
import org.kohsuke.github.GHRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

/**
 * Updates the SkillDB.
 * If an entity dosen't exist at the database or is outdated,
 * a new request to the GitHub API will be send via {@link GitHubCrawler}.
 * <p>
 * This service updates the {@link GitHubRepo}s.
 */
@Service
@Slf4j
public class GitHubRepoUpdater {
    private final GitHubCrawler gitHubCrawler;

    private final GitHubRepoRepository gitHubRepoRepository;

    @Autowired
    public GitHubRepoUpdater(final GitHubCrawler gitHubCrawler, final GitHubRepoRepository gitHubRepoRepository) {
        this.gitHubCrawler = gitHubCrawler;
        this.gitHubRepoRepository = gitHubRepoRepository;
    }

    @Async("repoExecutor")
    public CompletableFuture<ProcessingResult> insertOrUpdateRepo(final GHRepository ghRepository, final User user) {
        final Optional<GitHubRepo> optionalRepository = gitHubRepoRepository.findByNameAndUser(ghRepository.getName(), user);
        final GitHubRepo gitHubRepo;

        if (optionalRepository.isPresent()) {
            log.debug("gitHubRepo {} already exists", ghRepository.getName());
            gitHubRepo = optionalRepository.get();
        } else {
            log.info("create gitHubRepo {}", ghRepository.getName());
            gitHubRepo = gitHubRepoRepository.save(new GitHubRepo(ghRepository.getName(), user));
        }

        if (gitHubRepo.isOutdated()) {
            final Set<String> languages;
            try {
                languages = gitHubCrawler.getLanguagesOfRepo(ghRepository);
                gitHubRepo.setLanguages(languages);
                gitHubRepo.markAsUpdated();
                gitHubRepoRepository.save(gitHubRepo);
                log.info("finished update of gitHubRepo {}", gitHubRepo.getName());
            } catch (IOException | GHException e) {
                log.warn("failed to update gitHubRepo {}", ghRepository.getName());
                return CompletableFuture.completedFuture(ProcessingResult.FAILED);
            }
        }

        return CompletableFuture.completedFuture(ProcessingResult.SUCCESS);
    }
}
