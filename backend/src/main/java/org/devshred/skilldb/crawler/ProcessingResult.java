package org.devshred.skilldb.crawler;

public enum ProcessingResult {
    SUCCESS, FAILED
}
