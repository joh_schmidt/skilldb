package org.devshred.skilldb.service;

import com.google.common.collect.ImmutableSet;
import lombok.extern.slf4j.Slf4j;
import org.devshred.skilldb.domain.GitHubRepo;
import org.devshred.skilldb.domain.User;
import org.devshred.skilldb.repository.GitHubRepoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Prepares database content to display at the frontend.
 */
@Service
@Slf4j
public class UserService {
    private final GitHubRepoRepository gitHubRepoRepository;

    @Autowired
    public UserService(final GitHubRepoRepository gitHubRepoRepository) {
        this.gitHubRepoRepository = gitHubRepoRepository;
    }

    /**
     * Returns SkillDB-data grouped by {@link User}.
     * Each {@link User} entry contains all languages
     * the user has used ordered by the frequency this language
     * was used in different GitHub repos.
     *
     * @return {@link User}s w/ languages and frequency of usage
     */
    public Map<String, Map<String, Integer>> getUsers() {
        final Map<String, Map<String, Integer>> users = new HashMap<>();
        final Collection<GitHubRepo> repos = ImmutableSet.copyOf(gitHubRepoRepository.findAll());
        for (GitHubRepo repo : repos) {
            final Map<String, Integer> langCounter = users.computeIfAbsent(repo.getUser().getName(), o -> new HashMap<>());
            repo.getLanguages()
                    // get counter for each language
                    .forEach(lang -> langCounter
                            // increment counter
                            .merge(lang, 1, Integer::sum));
        }

        // remove users w/o languages (using an iterator to achieve this)
        // sort languages of each user by count desc
        for (Iterator<Map.Entry<String, Map<String, Integer>>> it = users.entrySet().iterator(); it.hasNext(); ) {
            final Map.Entry<String, Map<String, Integer>> user = it.next();
            if (user.getValue().isEmpty()) {
                it.remove();
                continue;
            }
            users.put(user.getKey(), user.getValue().entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new)));
        }
        return users;
    }
}
