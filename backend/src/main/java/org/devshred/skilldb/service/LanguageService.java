package org.devshred.skilldb.service;

import com.google.common.collect.ImmutableSet;
import lombok.extern.slf4j.Slf4j;
import org.devshred.skilldb.domain.GitHubRepo;
import org.devshred.skilldb.domain.User;
import org.devshred.skilldb.repository.GitHubRepoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class LanguageService {
    private final GitHubRepoRepository gitHubRepoRepository;

    @Autowired
    public LanguageService(final GitHubRepoRepository gitHubRepoRepository) {
        this.gitHubRepoRepository = gitHubRepoRepository;
    }

    /**
     * Returns SkillDB-data grouped by {@link User}.
     * Each {@link User} entry contains all languages
     * the user has used ordered by the frequency this language
     * was used in different GitHub repos.
     *
     * @return {@link User}s w/ languages and frequency of usage
     */
    public Map<String, Map<String, Integer>> getLanguages() {
        final Collection<GitHubRepo> repos = ImmutableSet.copyOf(gitHubRepoRepository.findAll());
        final Map<String, Map<String, Integer>> languages = new HashMap<>();
        for (GitHubRepo repo : repos) {
            for (String language : repo.getLanguages()) {
                final Map<String, Integer> userCounter = languages.computeIfAbsent(language, o -> new HashMap<>());
                userCounter.merge(repo.getUser().getName(), 1, Integer::sum);
            }
        }

        // sort users using a language by count desc
        for (Map.Entry<String, Map<String, Integer>> language : languages.entrySet()) {
            languages.put(language.getKey(), language.getValue().entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new)));
        }
        return languages;
    }
}
