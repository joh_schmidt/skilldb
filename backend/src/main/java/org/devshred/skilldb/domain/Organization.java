package org.devshred.skilldb.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Organization {
    private static final int DAYS_BEFORE_OUTDATED = 10;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    private LocalDateTime lastUpdated;

    public Organization(final String name) {
        this.name = name;
    }

    @Transient
    public boolean isOutdated() {
        return lastUpdated == null || lastUpdated.isBefore(LocalDateTime.now().minusDays(DAYS_BEFORE_OUTDATED));
    }

    @Transient
    public void markAsUpdated() {
        this.lastUpdated = LocalDateTime.now();
    }
}
