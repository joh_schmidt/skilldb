package org.devshred.skilldb.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GitHubRepo {
    private static final int DAYS_BEFORE_OUTDATED = 10;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    private LocalDateTime lastUpdated;

    @ManyToOne
    @JoinColumn(name = "fk_user")
    private User user;

    @Setter
    @ElementCollection(targetClass = String.class)
    private Set<String> languages = new HashSet<>();

    public GitHubRepo(final String name, final User user) {
        this.name = name;
        this.user = user;
        this.languages = new HashSet<>();
    }

    public GitHubRepo(final String name, final User user, final Set<String> languages) {
        this.name = name;
        this.user = user;
        this.languages = languages;
    }

    @Transient
    public boolean isOutdated() {
        return lastUpdated == null || lastUpdated.isBefore(LocalDateTime.now().minusDays(DAYS_BEFORE_OUTDATED));
    }

    @Transient
    public void markAsUpdated() {
        this.lastUpdated = LocalDateTime.now();
    }
}
