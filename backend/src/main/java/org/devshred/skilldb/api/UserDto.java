package org.devshred.skilldb.api;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserDto {
    private final String name;

    private final Map<String, Integer> languages;

    public static UserDto create(final String name, final Map<String, Integer> languages) {
        return new UserDto(name, languages);
    }
}
