package org.devshred.skilldb.api;

import org.devshred.skilldb.service.LanguageService;
import org.devshred.skilldb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

/**
 * REST-API to access data as stored at SkillDB.
 */
@RestController
@RequestMapping("/api")
public class SkillDbController {
    private final UserService userService;
    private final LanguageService languageService;

    @Autowired
    public SkillDbController(final UserService userService, final LanguageService languageService) {
        this.userService = userService;
        this.languageService = languageService;
    }

    /**
     * Returns SkillDB-data grouped by {@link UserDto}.
     * Each {@link UserDto} entry contains all languages
     * the user has used ordered by the frequency this language
     * was used in different GitHub repos.
     *
     * @return {@link UserDto}s w/ languages and frequency of usage
     */
    @GetMapping(value = "/users")
    public ResponseEntity users() {
        final Map<String, Map<String, Integer>> users = userService.getUsers();
        final List<UserDto> userDtos = users.entrySet().stream()
                .map(e -> UserDto.create(e.getKey(), e.getValue()))
                .sorted(Comparator.comparing(UserDto::getName))
                .collect(Collectors.toList());
        return ok().body(userDtos);
    }

    /**
     * Returns SkillDB-data grouped by {@link LanguageDto}.
     * Each {@link LanguageDto} entry contains all users
     * using this language ordered by the frequency this language
     * was used in different GitHub repos.
     *
     * @return {@link LanguageDto}s w/ users and frequency of usage
     */
    @GetMapping("/languages")
    public ResponseEntity languages() {
        final Map<String, Map<String, Integer>> languages = languageService.getLanguages();
        final List<LanguageDto> languageDtos = languages.entrySet().stream()
                .map(e -> LanguageDto.create(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
        return ok().body(languageDtos);
    }
}
