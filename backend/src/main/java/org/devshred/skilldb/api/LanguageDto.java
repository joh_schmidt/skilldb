package org.devshred.skilldb.api;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LanguageDto {
    private final String name;
    private final Map<String, Integer> users;

    public final static LanguageDto create(final String name, final Map<String, Integer> users) {
        return new LanguageDto(name, users);
    }
}
