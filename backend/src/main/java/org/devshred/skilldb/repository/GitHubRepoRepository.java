package org.devshred.skilldb.repository;

import org.devshred.skilldb.domain.GitHubRepo;
import org.devshred.skilldb.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface GitHubRepoRepository extends CrudRepository<GitHubRepo, Long> {
    Optional<GitHubRepo> findByNameAndUser(String name, User user);
}
