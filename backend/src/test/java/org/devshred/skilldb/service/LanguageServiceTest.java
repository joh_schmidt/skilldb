package org.devshred.skilldb.service;

import com.google.common.collect.ImmutableSet;
import org.devshred.skilldb.domain.GitHubRepo;
import org.devshred.skilldb.domain.Organization;
import org.devshred.skilldb.domain.User;
import org.devshred.skilldb.repository.GitHubRepoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LanguageServiceTest {
    @Mock
    private GitHubRepoRepository gitHubRepoRepository;

    @Test
    public void sortByUser() {
        final Organization organization = new Organization("SoftwareInc");
        final User user1 = new User("ppan", organization);
        final User user2 = new User("fbrick", organization);
        final User user3 = new User("llemon", organization);

        when(gitHubRepoRepository.findAll()).thenReturn(
                ImmutableSet.of(
                        new GitHubRepo("cool-stuff", user1, ImmutableSet.of("Lang4", "Lang3")),
                        new GitHubRepo("crazy-stuff", user1, ImmutableSet.of("Lang2", "Lang4")),
                        new GitHubRepo("fancy-stuff", user2, ImmutableSet.of("Lang3", "Lang2")),
                        new GitHubRepo("hot-stuff", user3, ImmutableSet.of("Lang1", "Lang2"))
                )
        );

        final LanguageService objectUnderTest = new LanguageService(gitHubRepoRepository);

        final Map<String, Map<String, Integer>> languages = objectUnderTest.getLanguages();

        assertThat(languages.size(), is(4));
        assertThat(languages.get("Lang2").size(), is(3));
        assertThat(languages.get("Lang4").size(), is(1));
        assertThat(languages.get("Lang4").get("ppan"), is(2));
    }
}