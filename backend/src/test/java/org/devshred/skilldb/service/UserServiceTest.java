package org.devshred.skilldb.service;

import com.google.common.collect.ImmutableSet;
import org.devshred.skilldb.domain.GitHubRepo;
import org.devshred.skilldb.domain.Organization;
import org.devshred.skilldb.domain.User;
import org.devshred.skilldb.repository.GitHubRepoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    private static final String USER_NAME = "ppan";
    private static final String USER_NAME_2 = "fbrick";
    @Mock
    private GitHubRepoRepository gitHubRepoRepository;

    private UserService objectUnderTest;

    @Before
    public void initMocks() {
        objectUnderTest = new UserService(gitHubRepoRepository);
    }

    @Test
    public void sortLanguages() {
        final Organization organization = new Organization("SoftwareInc");
        final User user = new User(USER_NAME, organization);
        when(gitHubRepoRepository.findAll()).thenReturn(
                ImmutableSet.of(
                        new GitHubRepo("cool-stuff", user, ImmutableSet.of("Perl", "Python")),
                        new GitHubRepo("crazy-stuff", user, ImmutableSet.of("Java", "JavaScript")),
                        new GitHubRepo("fancy-stuff", user, ImmutableSet.of("Java", "Python")),
                        new GitHubRepo("hot-stuff", user, ImmutableSet.of("Java"))
                )
        );
        final Map<String, Map<String, Integer>> users = objectUnderTest.getUsers();

        assertThat(users.size(), is(1));
        assertThat(users.get(USER_NAME).size(), is(4));
        assertThat(users.get(USER_NAME).values(), contains(3, 2, 1, 1));
    }

    @Test
    public void removeUsersWithoutLanguages() {
        final Organization organization = new Organization("SoftwareInc");
        final User user1 = new User(USER_NAME, organization);
        final User user2 = new User(USER_NAME_2, organization);
        when(gitHubRepoRepository.findAll()).thenReturn(
                ImmutableSet.of(
                        new GitHubRepo("cool-stuff", user1, ImmutableSet.of("Perl", "Python")),
                        new GitHubRepo("crazy-stuff", user2, ImmutableSet.of()),
                        new GitHubRepo("fancy-stuff", user1, ImmutableSet.of("Java", "Python"))
                )
        );
        final Map<String, Map<String, Integer>> users = objectUnderTest.getUsers();

        assertThat(users.size(), is(1));
        assertThat(users.containsKey(USER_NAME_2), is(false));
    }
}