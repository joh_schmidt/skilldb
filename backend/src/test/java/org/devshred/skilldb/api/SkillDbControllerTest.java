package org.devshred.skilldb.api;

import com.google.common.collect.ImmutableMap;
import org.devshred.skilldb.service.LanguageService;
import org.devshred.skilldb.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SkillDbController.class)
@ActiveProfiles("test")
public class SkillDbControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private LanguageService languageService;

    @Test
    public void returnsUserMapAsJson() throws Exception {
        when(userService.getUsers()).thenReturn(ImmutableMap.of("Peter", ImmutableMap.of("Java", 3, "Python", 2)));
        mockMvc
                .perform(get("/api/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"name\":\"Peter\",\"languages\":{\"Java\":3,\"Python\":2}}")));
    }
}