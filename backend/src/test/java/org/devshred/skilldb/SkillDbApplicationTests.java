package org.devshred.skilldb;

import org.devshred.skilldb.api.SkillDbController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SkillDbApplicationTests {

    @Autowired
    private SkillDbController skillDbController;

    @Test
    public void contexLoads() {
        assertThat(skillDbController, notNullValue());
    }
}
