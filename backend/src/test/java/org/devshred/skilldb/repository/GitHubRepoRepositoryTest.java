package org.devshred.skilldb.repository;

import org.devshred.skilldb.domain.GitHubRepo;
import org.devshred.skilldb.domain.Organization;
import org.devshred.skilldb.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class GitHubRepoRepositoryTest {
    private static final String ORGANIZATION = "SoftwareInc";
    private static final String USER_NAME = "ppan";
    private static final String REPO_NAME = "cool-stuff";

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private GitHubRepoRepository objectUndertest;

    @Test
    public void findGitHubRepository() {
        final Organization organization = entityManager.persist(new Organization(ORGANIZATION));
        final User user = entityManager.persist(new User(USER_NAME, organization));
        entityManager.persist(new GitHubRepo(REPO_NAME, user));

        final Optional<GitHubRepo> gitHubRepo = objectUndertest.findByNameAndUser(REPO_NAME, user);
        assertThat(gitHubRepo.isPresent(), is(true));
        assertThat(gitHubRepo.get().getName(), is(REPO_NAME));
    }

    @Test
    public void doNotReturnReposBelongingToAnotherUser() {
        final Organization organization = entityManager.persist(new Organization(ORGANIZATION));
        final User user1 = entityManager.persist(new User(USER_NAME, organization));
        final User user2 = entityManager.persist(new User(USER_NAME, organization));
        entityManager.persist(new GitHubRepo(REPO_NAME, user1));

        final Optional<GitHubRepo> gitHubRepo = objectUndertest.findByNameAndUser(REPO_NAME, user2);
        assertThat(gitHubRepo.isPresent(), is(false));
    }
}