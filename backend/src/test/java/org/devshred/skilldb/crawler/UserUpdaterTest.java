package org.devshred.skilldb.crawler;

import org.devshred.skilldb.domain.Organization;
import org.devshred.skilldb.domain.User;
import org.devshred.skilldb.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GHUser;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static java.util.Collections.singleton;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles(profiles = "non-async")
public class UserUpdaterTest {
    @Mock
    private GitHubCrawler gitHubCrawler;

    @Mock
    private UserRepository userRepository;

    @Mock
    private GitHubRepoUpdater gitHubRepoUpdater;

    @Mock
    private GHUser ghUserMock;

    @Mock
    private GHRepository ghRepositoryMock;

    @Mock
    private User userMock;

    @Mock
    private Organization organizationMock;

    private UserUpdater objectUnderTest;

    @Before
    public void init() {
        objectUnderTest = new UserUpdater(gitHubCrawler, userRepository, gitHubRepoUpdater);

        when(gitHubCrawler.getReposOfUser(ghUserMock)).thenReturn(singleton(ghRepositoryMock));
        when(userRepository.findByName(anyString())).thenReturn(Optional.of(userMock));
        when(userMock.isOutdated()).thenReturn(true);
        when(ghUserMock.getLogin()).thenReturn("loginName");
    }

    @Test
    public void saveUserIfEverythingWasFine() {
        when(gitHubRepoUpdater.insertOrUpdateRepo(eq(ghRepositoryMock), any(User.class)))
                .thenReturn(CompletableFuture.completedFuture(ProcessingResult.SUCCESS));

        objectUnderTest.insertOrUpdateUser(ghUserMock, organizationMock);

        verify(userRepository).save(any(User.class));
    }

    @Test
    public void doNotSaveUserIfSomethingFailed() {
        when(gitHubRepoUpdater.insertOrUpdateRepo(eq(ghRepositoryMock), any(User.class)))
                .thenReturn(CompletableFuture.completedFuture(ProcessingResult.FAILED));

        objectUnderTest.insertOrUpdateUser(ghUserMock, organizationMock);

        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    public void skipUpdateIfUserIsUpToDate() {
        when(userMock.isOutdated()).thenReturn(false);

        objectUnderTest.insertOrUpdateUser(ghUserMock, organizationMock);

        verify(gitHubRepoUpdater, never()).insertOrUpdateRepo(any(GHRepository.class), any(User.class));
    }
}