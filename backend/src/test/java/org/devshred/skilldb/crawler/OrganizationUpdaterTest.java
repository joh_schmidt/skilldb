package org.devshred.skilldb.crawler;

import org.devshred.skilldb.domain.Organization;
import org.devshred.skilldb.repository.OrganizationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kohsuke.github.GHUser;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static java.util.Collections.singleton;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles(profiles = "non-async")
public class OrganizationUpdaterTest {
    private static final String ORGANIZATION_NAME = "SoftwareInc";

    @Mock
    private GitHubCrawler gitHubCrawler;

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private UserUpdater userUpdater;

    @Mock
    private GHUser ghUserMock;

    @Mock
    private Organization organizationMock;

    private OrganizationUpdater objectUnderTest;

    @Before
    public void init() throws IOException {
        objectUnderTest = new OrganizationUpdater(gitHubCrawler, organizationRepository, userUpdater);

        when(organizationRepository.findByName(ORGANIZATION_NAME)).thenReturn(Optional.of(organizationMock));
        when(organizationRepository.save(any(Organization.class)))
                .thenAnswer((Answer<Organization>) invocationOnMock -> invocationOnMock.getArgument(0));
        when(gitHubCrawler.getMembersOfOrganization(ORGANIZATION_NAME)).thenReturn(singleton(ghUserMock));
        when(organizationMock.isOutdated()).thenReturn(true);
    }

    @Test
    public void saveOrgIfEverythingWasFine() {
        when(userUpdater.insertOrUpdateUser(eq(ghUserMock), any(Organization.class)))
                .thenReturn(CompletableFuture.completedFuture(ProcessingResult.SUCCESS));

        objectUnderTest.insertOrUpdateOrganization(ORGANIZATION_NAME);

        verify(organizationRepository).save(any(Organization.class));
    }

    @Test
    public void doNotSaveOrgIfSomethingFailed() {
        when(userUpdater.insertOrUpdateUser(eq(ghUserMock), any(Organization.class)))
                .thenReturn(CompletableFuture.completedFuture(ProcessingResult.FAILED));

        objectUnderTest.insertOrUpdateOrganization(ORGANIZATION_NAME);

        verify(organizationRepository, never()).save(any(Organization.class));
    }

    @Test
    public void skipUpdateIfOrgIsUpToDate() {
        when(organizationMock.isOutdated()).thenReturn(false);

        objectUnderTest.insertOrUpdateOrganization(ORGANIZATION_NAME);

        verify(userUpdater, never()).insertOrUpdateUser(any(GHUser.class), any(Organization.class));
    }
}