package org.devshred.skilldb.crawler;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;
import org.kohsuke.github.GHPerson;
import org.kohsuke.github.GHUser;
import org.kohsuke.github.GitHubBuilder;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class GitHubCrawlerTest {
    private static final String ORG_RESPONSE = "{\n" +
            "  \"login\": \"SoftwareInc\",\n" +
            "  \"id\": 1009716,\n" +
            "  \"url\": \"https://api.github.com/orgs/SoftwareInc\",\n" +
            "  \"repos_url\": \"https://api.github.com/orgs/SoftwareInc/repos\",\n" +
            "  \"events_url\": \"https://api.github.com/orgs/SoftwareInc/events\",\n" +
            "  \"hooks_url\": \"https://api.github.com/orgs/SoftwareInc/hooks\",\n" +
            "  \"issues_url\": \"https://api.github.com/orgs/SoftwareInc/issues\",\n" +
            "  \"members_url\": \"https://api.github.com/orgs/SoftwareInc/members{/member}\",\n" +
            "  \"public_members_url\": \"https://api.github.com/orgs/SoftwareInc/public_members{/member}\",\n" +
            "  \"avatar_url\": \"https://avatars3.githubusercontent.com/u/1009716?v=4\",\n" +
            "  \"description\": null,\n" +
            "  \"name\": \"Software Inc.\",\n" +
            "  \"company\": null,\n" +
            "  \"blog\": \"blog.software-inc.com\",\n" +
            "  \"location\": null,\n" +
            "  \"email\": null,\n" +
            "  \"has_organization_projects\": true,\n" +
            "  \"has_repository_projects\": true,\n" +
            "  \"public_repos\": 138,\n" +
            "  \"public_gists\": 0,\n" +
            "  \"followers\": 0,\n" +
            "  \"following\": 0,\n" +
            "  \"html_url\": \"https://github.com/SoftwareInc\",\n" +
            "  \"created_at\": \"2011-08-28T09:54:00Z\",\n" +
            "  \"updated_at\": \"2018-02-05T08:21:42Z\",\n" +
            "  \"type\": \"Organization\"\n" +
            "}";

    private static final String MEMBERS_RESPONSE = " [{\n" +
            "    \"login\": \"ppan\",\n" +
            "    \"id\": 1261007,\n" +
            "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/1261007?v=4\",\n" +
            "    \"gravatar_id\": \"\",\n" +
            "    \"url\": \"https://api.github.com/users/ppan\",\n" +
            "    \"html_url\": \"https://github.com/ppan\",\n" +
            "    \"followers_url\": \"https://api.github.com/users/ppan/followers\",\n" +
            "    \"following_url\": \"https://api.github.com/users/ppan/following{/other_user}\",\n" +
            "    \"gists_url\": \"https://api.github.com/users/ppan/gists{/gist_id}\",\n" +
            "    \"starred_url\": \"https://api.github.com/users/ppan/starred{/owner}{/repo}\",\n" +
            "    \"subscriptions_url\": \"https://api.github.com/users/ppan/subscriptions\",\n" +
            "    \"organizations_url\": \"https://api.github.com/users/ppan/orgs\",\n" +
            "    \"repos_url\": \"https://api.github.com/users/ppan/repos\",\n" +
            "    \"events_url\": \"https://api.github.com/users/ppan/events{/privacy}\",\n" +
            "    \"received_events_url\": \"https://api.github.com/users/ppan/received_events\",\n" +
            "    \"type\": \"User\",\n" +
            "    \"site_admin\": false\n" +
            "  }, {\n" +
            "    \"login\": \"fbrickowski\",\n" +
            "    \"id\": 1261007,\n" +
            "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/1261007?v=4\",\n" +
            "    \"gravatar_id\": \"\",\n" +
            "    \"url\": \"https://api.github.com/users/fbrickowski\",\n" +
            "    \"html_url\": \"https://github.com/fbrickowski\",\n" +
            "    \"followers_url\": \"https://api.github.com/users/fbrickowski/followers\",\n" +
            "    \"following_url\": \"https://api.github.com/users/fbrickowski/following{/other_user}\",\n" +
            "    \"gists_url\": \"https://api.github.com/users/fbrickowski/gists{/gist_id}\",\n" +
            "    \"starred_url\": \"https://api.github.com/users/fbrickowski/starred{/owner}{/repo}\",\n" +
            "    \"subscriptions_url\": \"https://api.github.com/users/fbrickowski/subscriptions\",\n" +
            "    \"organizations_url\": \"https://api.github.com/users/fbrickowski/orgs\",\n" +
            "    \"repos_url\": \"https://api.github.com/users/fbrickowski/repos\",\n" +
            "    \"events_url\": \"https://api.github.com/users/fbrickowski/events{/privacy}\",\n" +
            "    \"received_events_url\": \"https://api.github.com/users/fbrickowski/received_events\",\n" +
            "    \"type\": \"User\",\n" +
            "    \"site_admin\": false\n" +
            "  }\n" +
            "]";
    @Rule
    public final WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());

    @Test
    public void fetchMembersFromGithub() throws IOException {
        stubFor(get(urlEqualTo("/orgs/SoftwareInc")).willReturn(aResponse().withBody(ORG_RESPONSE)));
        stubFor(get(urlEqualTo("/orgs/SoftwareInc/members")).willReturn(aResponse().withBody(MEMBERS_RESPONSE)));

        final GitHubCrawler objectUnderTest = new GitHubCrawler(
                new GitHubBuilder().withEndpoint("http://localhost:" + wireMockRule.port()).build());

        final Collection<GHUser> membersOfOrganization = objectUnderTest.getMembersOfOrganization("SoftwareInc");
        final List<String> logins = membersOfOrganization.stream().map(GHPerson::getLogin).collect(Collectors.toList());

        assertThat(logins, contains("ppan", "fbrickowski"));
    }
}