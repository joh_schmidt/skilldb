package org.devshred.skilldb.crawler;

import org.devshred.skilldb.domain.GitHubRepo;
import org.devshred.skilldb.domain.User;
import org.devshred.skilldb.repository.GitHubRepoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kohsuke.github.GHRepository;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.util.Optional;

import static java.util.Collections.singleton;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles(profiles = "non-async")
public class GitHubRepoUpdaterTest {
    private static final String LANGUAGE = "Java";
    private static final String REPO_NAME = "repo";

    @Mock
    private GitHubCrawler gitHubCrawler;

    @Mock
    private GitHubRepoRepository gitHubRepoRepository;

    @Mock
    private GHRepository ghRepositoryMock;

    @Mock
    private User userMock;

    @Mock
    private GitHubRepo gitHubRepoMock;

    private GitHubRepoUpdater objectUnderTest;

    @Before
    public void init() throws IOException {
        objectUnderTest = new GitHubRepoUpdater(gitHubCrawler, gitHubRepoRepository);

        when(ghRepositoryMock.getName()).thenReturn(REPO_NAME);
        when(gitHubCrawler.getLanguagesOfRepo(ghRepositoryMock)).thenReturn(singleton(LANGUAGE));
        when(gitHubRepoRepository.findByNameAndUser(REPO_NAME, userMock)).thenReturn(Optional.of(gitHubRepoMock));
    }

    @Test
    public void updateIfRepoIsOudated() {
        when(gitHubRepoMock.isOutdated()).thenReturn(true);

        objectUnderTest.insertOrUpdateRepo(ghRepositoryMock, userMock);

        verify(gitHubRepoRepository).save(any(GitHubRepo.class));
    }

    @Test
    public void skipUpdateIfRepoIsUpToDate() {
        when(gitHubRepoMock.isOutdated()).thenReturn(false);

        objectUnderTest.insertOrUpdateRepo(ghRepositoryMock, userMock);

        verify(gitHubRepoRepository, never()).save(any(GitHubRepo.class));
    }
}