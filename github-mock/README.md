GitHub Mock
===========

Um das Rate Limit der GitHub API zu umgehen, kann ein einfacher Mock verwendet werden.

Das Skript crawl.sh lädt die für die SkillDB benötigten Daten und legt diese im Unterverzeichnis _site_ ab. Sobald das Rate Limit erreicht ist, beendet sich das Skript. Das Skript lädt nur noch nicht vorhandene Daten, lädt also bei wiederholtem Start nach und nach alle von der GitHub API zur Verfügung gestellten Daten.
 
Alternativ zum Script kann auch die Datei _site.tar.bz2_ entpackt werden; diese enthält bereits alle Daten.

Als Mock-Server wird ein nginx-Container verwendet, welcher die Daten in _site_ ausliefert und zusätlich folgende Anpassungen vornimmt:
 * Dateien der Art /pfad/zur/datei sind unter /pfad/zur/datei/index erreichbar.
 * In den Dateien werden alle Verweise auf https://api.github.com durch URIs des Mock-Servers ersetzt.
 
Der Mock-Server kann mittels des Skriptes _nginx.sh_ gestartet werden.

_Die Adresse des Mock-Servers (http://localhost:7000) muss anschließend in der application.yml konfigriert werden._