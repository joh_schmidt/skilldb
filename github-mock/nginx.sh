#!/bin/bash

# serves files of a given directory
# overwrites default configuration w/ local default.conf

CONTAINER=nginx-github-mock

docker stop $CONTAINER && docker rm $CONTAINER

docker run \
	--name $CONTAINER \
	-v $(pwd)/default.conf:/etc/nginx/conf.d/default.conf:ro \
	-v $(pwd)/site:/usr/share/nginx/html:ro \
	-p 7000:80 \
	-d \
	nginx:alpine

docker logs -f $CONTAINER