#!/bin/bash

# crawls the GitHub API and stores all
# 	* members
# 	* repos of the members
# 	* langauages of these repos
# 	of a given organization to disk
# exits if a request fails due to API rate limit exceeded
# skips previously requested pages

# name of the organization on GitHub
organization=codecentric

# the curl-command to use
CURL="curl -s"
#CURL="curl -H \"Authorization: token *****\" -s"

# the basedir of all stored files on local disk
local_base_dir="site/"

# saves the given url to disc
# param1: the url of the resource to save
# param2: (optional) adding this suffix to the filename of the saved resource
function requestAndSave {
	url=$1
	suffix=$2
	path=$(getPathFromUrl $url)
	base_path=$(getBasePathFromPath $path)
	if [[ -n "$2" ]]; then
		path=$path/$2
	fi
	if [[ -f $path ]]; then
		echo "$path already exists"
		return
	else
		echo "creating $path"
	fi
	base_path=$(getBasePathFromPath $path)
	mkdir -p $base_path
	$CURL $url > $path
	checkIfRateLimitExceeded
}

function getPathFromUrl {
	echo -ne $local_base_dir
	echo $1 | sed -e 's/https:\/\/api.github.com\///'
}

function getBasePathFromPath {
	echo $1 | sed 's%/[^/]*$%/%'
}

# checks if the last request failed due to rate limit exeeded
function checkIfRateLimitExceeded {
	grep "API rate limit exceeded" $path > /dev/null
	if [[ $? -eq 0 ]]; then
		echo API rate limit exceeded
		rm $path
		reset_at=`curl -s https://api.github.com/rate_limit | jq .rate.reset`
		echo "next reset at $(date -r $reset_at)"
		exit
	fi
}

# get the organization and their members
requestAndSave https://api.github.com/orgs/$organization index
requestAndSave https://api.github.com/orgs/$organization/members

# get the details page of each member
for memberUrl in `cat ${local_base_dir}orgs/$organization/members | jq -r '.[].url'`; do
	requestAndSave $memberUrl index
done

# get the repos (with languages used) of each member
for reposUrl in `cat ${local_base_dir}orgs/$organization/members | jq -r '.[].repos_url'`; do
	requestAndSave $reposUrl
	path_to_member=$(getPathFromUrl $reposUrl)
	for languagesUrl in `cat ${path_to_member} | jq -r '.[].languages_url'`; do
		requestAndSave $languagesUrl
	done
done

echo done