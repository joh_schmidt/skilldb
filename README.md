SkillDB
=======

[ ![Codeship Status for joh_schmidt/skilldb](https://app.codeship.com/projects/1cb8a2a0-222a-0136-42a8-42953336982e/status?branch=master)](https://app.codeship.com/projects/285990)

SkillDB fasst die öffentlich verfügbaren GitHub-Repositories und die darin verwendeten Programmiersprachen der einzelnen Mitarbeiter in einer Datenbank zusammen und stellt einfache Ansichten dieser Daten zur Verfügung.

Datenbasis
----------
Die Anwendung zieht die Daten von der GitHub REST API und speichert diese in einer internen H2-Datenbank. 
Stündlich wird ein Update der Daten angestoßen (ebenfalls stündlich erfolgt ein Reset des Rate Limits der GitHub API).
Hierbei werden nur Daten von der GitHub API angefragt, die noch nicht in der Datenbank gespeichert wurden order älter als 10 Tage sind.

Um das Rate Limit (60 Requests pro Stunde) zu umgehen und die Datenbank möglichst schnell zu füllen, gibt es zwei Alternativen:
 - Nutzen eines API-Mocks (siehe [github-mock](github-mock))
 - Einspielen eines DB-Dumps (siehe [backup](backup))

Technologie
-----------
Die Anwendung basiert auf einem Backend auf Basis von Spring Boot und einem Vue.js-Frontend.
Die Daten werden in einer H2-Datenbank gespeichert. 
Beim Zugriff auf die GitHub REST API wird die Clientbibliothek von Kohsuke Kawaguchi verwendet. 

Bauen und Starten
-----------------
Die Anwendung kann wie folgt gebaut und gestartet werden:
```
mvn clean install
mvn --projects backend spring-boot:run
```
… und kann anschließend unter http://localhost:7001/ aufgerufen werden.

Der Frontend-Code wird in diesem Fall vom Backend-Server ausgeliefert. Für die Verwendung des webpack-dev-server siehe [frontend](frontend).

Offene Punkte
-------------
 - Mögliche Fehler beim Abrufen der GitHub REST API werden vermutlich nicht vollständig abgedeckt. Aktuell werden
     - beim Erreichen des Rate Limits alle weiteren Verbindungen unterbunden
     - bei der Abfrage der Sprachen eines Projekts wird ab zwei fehlgeschlagenen Verbindungen ein CircuitBreaker geöffnet
     - … weitere Fehlermöglichkeiten müssen noch behandelt werden
     - außerdem sollte noch ein Retry-Mechanismus genutzt werden
 - Angesichts des Rate Limits ist die Parallelisierung beim Abfragen der GitHub API zumindest fraglich.
 - Im Frontend gibt es aktuell keine Tests. 