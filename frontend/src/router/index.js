import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Mitarbeiter from '@/components/Mitarbeiter'
import Suche from '@/components/Suche'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Hello
    },
    {
      path: '/hello',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/mitarbeiter',
      name: 'Mitarbeiter',
      component: Mitarbeiter
    },
    {
      path: '/suche',
      name: 'Suche',
      component: Suche
    }
  ]
})
