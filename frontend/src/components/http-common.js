import axios from 'axios'

export const AXIOS = axios.create({
  baseURL: `http://localhost:7001/api`,
  headers: {
    'Access-Control-Allow-Origin': 'http://localhost:3000'
  }
})
