SkillDB::Frontend
=================
Starten des webpack-dev-server
------------------------------
Für schnelles live reloading während der Entwicklung kann der webpack-dev-server genutzt werden.
```
cd frontend
npm run dev
```
Die Anwendung ist nun unter http://localhost:3000 erreichbar.
