#!/bin/bash

# restores the file skilldb.zip

if [[ ! -f h2-1.4.197.jar ]]; then
    curl http://repo2.maven.org/maven2/com/h2database/h2/1.4.197/h2-1.4.197.jar --output h2-1.4.197.jar --silent
fi

java -cp *.jar org.h2.tools.RunScript -url jdbc:h2:file:/tmp/skilldb -user sa -script skilldb.zip -options compression zip