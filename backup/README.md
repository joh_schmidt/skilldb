Backup/ Restore
===============

Um die SkillDB ohne initiales Crawlen der GitHub-API oder des Mock-Servers nutzen zu können, kann die der Anwendung zugrunde liegende H2-Datenbank auch direkt befüllt werden.

Das Skript _restore.sh_ lädt den benötigten DB-Treiber und importiert anschließend die Daten.

_Die Datenbank-URL muss natürlich mit dem in der application.yml konfigurierten Wert übereinstimmen._